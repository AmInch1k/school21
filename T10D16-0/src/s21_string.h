#ifndef S21_STRING_H
#define S21_STRING_H
int s21_strlen(char *str);
int s21_strcmp(char *str1, char *str2);
char *s21_strcpy(char *str1, char *str2);
#endif
